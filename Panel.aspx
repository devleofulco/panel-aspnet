﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Panel.aspx.cs" Inherits="Painel.Panel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>PANEL EM .ASPNET</title>
    <style>
        body {
            background-color: #f0f0f0;
            font-family:fantasy;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }
        .form-container {
            background: linear-gradient(to top right, #03ff00, #FFFFFF);
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.1);
            width: 500px;
        }
        .form-group {
            margin-bottom: 15px;
            
        }
        .form-group label {
            display: block;
            margin-bottom: 5px;
        }
        .form-group input, .form-group select {
            width: 90%;
            padding: 10px;
            border-radius: 4px;
            border: 3px solid #000000;
            margin-bottom: 10px;
        }
        .form-group input[type="submit"] {
            width: 100%;
            border-radius: 12px;
            background-color: #007BFF;
            color: white;
            cursor: pointer;
        }
        .form-group input[type="submit"]:hover {
            background-color: #9370DB;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" class="form-container">
      <asp:Panel ID="Panel1" runat="server" GroupingText="Exemplo de Panel em ASP.NET">
        <asp:Panel ID="Panel2" runat="server" GroupingText="INFORMAÇÕES PESSOAIS" CssClass="form-group">
        Nome: <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><br />
        Sobrenome: <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox><br />
        Genero: <asp:DropDownList ID="DropDownList1" runat="server">
                <asp:ListItem Text="Masculino" Value="Masculino"></asp:ListItem>
                <asp:ListItem Text="Feminino" Value="Feminino"></asp:ListItem>
                <asp:ListItem Text="Outro" Value="Outro"></asp:ListItem>
                </asp:DropDownList><br />
        Telefone: <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox><br />
        <asp:Button ID="Button1" runat="server" Text="Próximo" OnClick="Button1_Click" />
    </asp:Panel>
    <asp:Panel ID="Panel3" runat="server" GroupingText="DETALHE DE ENDEREÇO" CssClass="form-group">
        Endereço: <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox><br />
        Cidade: <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox><br />
        CEP: <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox><br />
        <asp:Button ID="Button2" runat="server" Text="Voltar" OnClick="Button2_Click" />
        <asp:Button ID="Button3" runat="server" Text="Próximo" OnClick="Button3_Click" />
    </asp:Panel>
    <asp:Panel ID="Panel4" runat="server" GroupingText="AREA DE LOGIN" CssClass="form-group">
        Usuário: <asp:TextBox ID="TextBox8" runat="server" OnTextChanged="TextBox8_TextChanged"></asp:TextBox><br />
        Senha: <asp:TextBox ID="TextBox9" runat="server" TextMode="Password"></asp:TextBox><br />
        <asp:Button ID="Button4" runat="server" Text="Voltar" OnClick="Button4_Click" />
        <asp:Button ID="Button5" runat="server" Text="Enviar" OnClick="Button5_Click" />
    </asp:Panel>
</asp:Panel>
    </form>
</body>
</html>
