﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Painel
{
    public partial class Panel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Panel2.Visible = true;
                Panel3.Visible = false;
                Panel4.Visible = false;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Panel2.Visible = false;
            Panel3.Visible = true;
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Panel3.Visible = false;
            Panel2.Visible = true;
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Panel3.Visible = false;
            Panel4.Visible = true;
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            Panel4.Visible = false;
            Panel3.Visible = true;
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            
            Table table = new Table { CssClass = "center" };
            table.Style.Add(HtmlTextWriterStyle.MarginLeft, "auto");
            table.Style.Add(HtmlTextWriterStyle.MarginRight, "auto");

            
            AddRowToTable(table, "Nome", TextBox1.Text);
            AddRowToTable(table, "Sobrenome", TextBox2.Text);
            AddRowToTable(table, "Genero", DropDownList1.Text);
            AddRowToTable(table, "Telefone", TextBox4.Text);
            AddRowToTable(table, "Endereço", TextBox5.Text);
            AddRowToTable(table, "Cidade", TextBox6.Text);
            AddRowToTable(table, "CEP", TextBox7.Text);
            AddRowToTable(table, "Usuário", TextBox8.Text);
            AddRowToTable(table, "Senha", TextBox9.Text);

            
            Panel4.Controls.Add(table);
        }

        private void AddRowToTable(Table table, string labelText, string valueText)
        {
            TableRow row = new TableRow();
            TableCell labelCell = new TableCell { Text = labelText };
            TableCell valueCell = new TableCell { Text = valueText };
            row.Cells.Add(labelCell);
            row.Cells.Add(valueCell);
            table.Rows.Add(row);
        }

        protected void TextBox8_TextChanged(object sender, EventArgs e)
        {

        }
    }
}